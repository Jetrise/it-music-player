
/**
 * Class: generalController
 * Description: Handles general program operations.
 * Author: Shaun Cockram
 * 
 * PARAMETERS:
 * 
 * RETURNS:
 * 
 * - String: APPROOT
 */

public class generalController
{
    String APPROOT; 
    public String getAppRootDir()
    {
        try {
            APPROOT = System.getProperty("user.dir");
        }catch(Exception e){
            e.printStackTrace();
        }
        return APPROOT; 
    }
}

import java.sql.*;
public class Database{
    private Connection con;
    private Statement stmt;
    private ResultSet RS;
    public  void Database()
    {
        try {
            //connecting to MySQL database on thorin
            Class.forName("com.mysql.jdbc.Driver");    
            con = DriverManager.getConnection("jdbc:mysql://thorin.stbrn.ac.uk/150328","150328","123"); //database url, user, password
            System.out.println("Successfully connected to thorin MYSQL database");
            //            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/jaztest","root","");   //defaults: port number 3306, user root, no password
            //            System.out.println("Successfully connected to local MYSQL database");
            /**Set up Statement object to return a scrollable and updatable resultSet  */      
            stmt=con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);    
        }//end of try

        catch  (Exception e) {
            System.out.println("Error: error connecting to database because : " + e.getMessage());
        }
    }

    private Connection getCon(){
        return con;
    }
    
    private Statement getStmt(){
        return stmt;
    }
}
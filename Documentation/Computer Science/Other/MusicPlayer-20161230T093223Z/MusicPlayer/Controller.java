
import java.util.concurrent.TimeUnit;//for time
import java.util.Scanner; //Used for user inputs
import java.io.File; //used for file chooser
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;

public class Controller implements ActionListener
{
    public static String name = System.getProperty("user.name");

    // public static String name = "Shaun";
    public static String excep = " ";
    public static String filename;
    //     public static String filepath;
    public static String response = "Menu";
    public static String exisitingfolder;
    public static String loadfolder = " ";
    public static int i = 0;
    public static Clip clip;
    public static File[] listOfFiles;
    public static String musicfilepath;
    public static File soundFile;
    public static AudioInputStream sound;
    public static DataLine.Info info;
    public static boolean playing = false;
    public static String filepath = "S:\\Computer Science\\Student_Shared_Area\\2015 16 submissions\\AS mini project 5\\Shaun C - MusicPlayer - MP5\\User Playlists\\" + name;
    private int x;

    JFrame f;

    public Controller()
    {      
        x = 0;
    }

    public Controller (JFrame jfIn)
    {
        f = jfIn;
        GUIPlayerOrig.itemC.addActionListener(this);
        GUIPlayerOrig.itemL.addActionListener(this);
        GUIPlayerOrig.itemEP.addActionListener(this);
        GUIPlayerOrig.playButton.addActionListener(this);
        GUIPlayerOrig.stopButton.addActionListener(this);
        GUIPlayerOrig.nextButton.addActionListener(this);
        GUIPlayerOrig.prevButton.addActionListener(this);
        GUIPlayerOrig.debugger.addActionListener(this);

    }

    public void actionPerformed(ActionEvent e)  { 
        if (e.getSource() == GUIPlayerOrig.itemC) 

            try{
                createfolder(); //calling create folder method
            }
            catch (Exception ex){}

        if (e.getSource() == GUIPlayerOrig.itemL) 

            try{
                loadfolder(); //calling load folder method
            }
            catch (Exception ex){}

        if (e.getSource() == GUIPlayerOrig.itemEP) 

            try{
                loadfolderexample(); //calling load folder method
            }
            catch (Exception ex){}

        if (e.getSource() == GUIPlayerOrig.playButton) 

            try{
                playsong(); //calling load folder method
            }
            catch (Exception ex){}

        if (e.getSource() == GUIPlayerOrig.stopButton) 

            try{
                stopsong(); //calling load folder method
            }
            catch (Exception ex){}

        if (e.getSource() == GUIPlayerOrig.nextButton) 

            try{
                nextsong(); //calling load folder method
            }
            catch (Exception ex){}

        if (e.getSource() == GUIPlayerOrig.prevButton) 

            try{
                prevsong(); //calling load folder method
            }
            catch (Exception ex){}

        if (e.getSource() == GUIPlayerOrig.debugger) 

            try{
                debugger(); //calling load folder method
            }
            catch (Exception ex){}

        //This opens file explorer on a specific path.

    }

    public static void createfolder() throws Exception
    {

        /**
         * Below is essentially how you make a folder. However, it also allows you to delete previous folders with the name that you inputted at the vry start.
         */ 
        File f = new File(filepath);
        do
        {

            //String filepath = "E:\\MiniProject\\" + name; //HOME TESTING

            try{
                if(f.mkdir()) { 
                    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                    Date date = new Date();
                    String tango = "Log File (" + name + ").txt";
                    AQAWriteTextFile2015 writer = new AQAWriteTextFile2015(tango, true);
                    String log = "DEBUG FOLDER CREATE (" + date + "): Folder Created with name:" + name;
                    writer.writeToTextFile(log);
                    writer.closeFile();

                    excep = "Pass";
                } else {
                    //                     System.out.println("\n\n" + "//DEBUG: Folder could not be created. This may be because there is already a folder with that name." + "\n\n" + "If this is the case, delete it and press enter when you are ready to continue");

                    JDialog.setDefaultLookAndFeelDecorated(true);
                    int response = JOptionPane.showConfirmDialog(null, "Folder could not be created. This may be because there is already a folder with that name. Do you want to delete the folder with your current inputted name and create a new foler?", "Error", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                    if (response == JOptionPane.NO_OPTION) {
                        System.out.println("Please delete the foler manually or alternatively, re-run the program and use a different name.");
                    } else if (response == JOptionPane.YES_OPTION) {

                        try{
                            //String name1 = System.getProperty("user.name");
                            //String filepath1 = "S:\\Computer Science\\Student_Shared_Area\\2015 16 submissions\\AS mini project 5\\Shaun C - MusicPlayer - MP5\\User Playlists\\" + name;
                            //File deletefolder = new File(filepath1);
                            if(f.delete()){
                                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                                Date date = new Date();
                                String tango = "Log File (" + name + ").txt";
                                AQAWriteTextFile2015 writer = new AQAWriteTextFile2015(tango, true);
                                String log = "DEBUG FOLDER DELETE (" + date + "): Folder, '" + f.getName() + "' is deleted!";
                                writer.writeToTextFile(log);
                                writer.closeFile();

                            }else{

                                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                                Date date = new Date();
                                String tango = "Log File (" + name + ").txt";
                                AQAWriteTextFile2015 writer = new AQAWriteTextFile2015(tango, true);
                                String log = "DEBUG FOLDER DELETE (" + date + "): Delete operation has failed. #ProbablyJustDeletedSharedDrive";
                                writer.writeToTextFile(log);
                                writer.closeFile();
                            }

                        }catch(Exception e){

                            e.printStackTrace();

                        }

                    } else if (response == JOptionPane.CLOSED_OPTION) {
                        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                        Date date = new Date();
                        String tango = "Log File (" + name + ").txt";
                        AQAWriteTextFile2015 writer = new AQAWriteTextFile2015(tango, true);
                        String log = "DEBUG JPANEL (" + date + "): JPane was closed by user.";
                        writer.writeToTextFile(log);
                        writer.closeFile();
                        excep = "Fail";
                    }

                    excep = "Fail";
                }
            }      catch(Exception e){
                e.printStackTrace();
            } 

            //loadfolder = "S:\\Computer Science\\Student_Shared_Area\\2015 16 submissions\\AS mini project 4\\MiniProject4 - Shaun Cockram\\Playlists\\" + name;
            loadfolder = filepath;
            //loadfolder = "E:\\MiniProject\\" + name; //HOME TESTING
        }
        while (excep.equalsIgnoreCase("Fail"));

        loadfolder = filepath;
        //loadfolder = "E:\\MiniProject\\" + name; //HOME TESTING

        Runtime.getRuntime().exec("explorer.exe /select," + loadfolder);
        TimeUnit.SECONDS.sleep(1); //Doesn't run the next line of code until 1 second has passed.

        JOptionPane.showMessageDialog(null, "File Explorer has just opened. On the highlighted folder, please double click and place your music files in your folder." + "\n\n" + "They must be in WAV format and there must be at least 1 file. Once you have done that, come back to the terminal window.");
    }

    public static void loadfolder() throws Exception
    {
        try{
            loadfolder = filepath;
            //loadfolder = "E:\\MiniProject\\" + name; //HOME TESTING
            File f = new File(loadfolder);
            if(f.exists() ) { 
                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Date date = new Date();
                String tango = "Log File (" + name + ").txt";
                AQAWriteTextFile2015 writer = new AQAWriteTextFile2015(tango, true);
                String log = "DEBUG FOLDER LOAD (" + date + "): Folder loaded with name: " + name;
                writer.writeToTextFile(log);
                writer.closeFile();
            }
            else {
                //System.out.println("//DEBUG LOAD: Folder could not be loaded with name: " + name + ". This is because there is no folder called " + name + " in the directory. Please use the create option");

                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Date date = new Date();
                String tango = "Log File (" + name + ").txt";
                AQAWriteTextFile2015 writer = new AQAWriteTextFile2015(tango, true);
                String log = "DEBUG FOLDER LOAD (" + date + "): Folder could not be loaded with name: " + name + ". This is because there is no folder called " + name + " in the directory. Please use the create option";
                writer.writeToTextFile(log);
                writer.closeFile();
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
        // && !f.isDirectory())

    }

    public static void loadfolderexample() throws Exception
    {
        try{

            loadfolder = "Playlists\\";
            File f = new File(loadfolder);
            if(f.exists() ) { 

                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Date date = new Date();
                String tango = "Log File (" + name + ").txt";
                AQAWriteTextFile2015 writer = new AQAWriteTextFile2015(tango, true);
                String log = "DEBUG FOLDER LOAD (" + date + "): Example Folder Loaded";
                writer.writeToTextFile(log);
                writer.closeFile();
            }
            else {
                System.out.println("//DEBUG LOAD: Some melon has deleted the Example Songs folder.");
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
        // && !f.isDirectory())

    }

    public static void playsong() throws Exception
    {
        if (playing == true) 
        {
            clip.stop();

            clip.close();
            clip = null;

        }
        else
        {

        }

        File folder = new File(loadfolder);
        listOfFiles = folder.listFiles();
        int k;
        System.out.println("----------------------------------------------------------------------");
        System.out.println("List of Songs:");
        System.out.println("");
        for (k = 0; k < listOfFiles.length; k++) {
            if (listOfFiles[k].isFile()) {
                System.out.println("Song "+ k +"\t" + listOfFiles[k].getName());
            } else if (listOfFiles[k].isDirectory()) {
                System.out.println("Directory " + listOfFiles[k].getName());
            }
        } 

        //         i = 0;
        musicfilepath = loadfolder + "\\" + listOfFiles[i].getName();

        soundFile = new File(musicfilepath);
        sound = AudioSystem.getAudioInputStream(soundFile);
        info = new DataLine.Info(Clip.class, sound.getFormat());
        clip = (Clip) AudioSystem.getLine(info);
        clip.open(sound);

        clip.start();
        playing = true;
        System.out.println("----------------------------------------------------------------------");
        System.out.println("Song " + i + " ,'" + listOfFiles[i].getName() + "' started playing");
        System.out.println("----------------------------------------------------------------------");
        GUIPlayerOrig.label.setText("Song " + i + " ,'" + listOfFiles[i].getName() + "' started playing");
    }

    public static void stopsong() throws Exception
    {
        clip.stop();
        TimeUnit.SECONDS.sleep(1);
        clip.close();
        clip = null;
        playing = false;
        GUIPlayerOrig.label.setText("Song " + i + " ,'" + listOfFiles[i].getName() + "' stopped playing");
        //System.exit(0);
    }

    public static void nextsong() throws Exception
    {
        clip.stop();     
        clip.close();
        clip = null;
        i = i + 1;
        if (i == listOfFiles.length) i=0;
        musicfilepath = loadfolder + "\\" + listOfFiles[i].getName();
        soundFile = new File(musicfilepath);
        sound = AudioSystem.getAudioInputStream(soundFile);
        info = new DataLine.Info(Clip.class, sound.getFormat());
        clip = (Clip) AudioSystem.getLine(info);
        clip.open(sound);
        clip.start();
        playing = true;
        GUIPlayerOrig.label.setText("Song " + i + " ,'" + listOfFiles[i].getName() + "' started playing");
    }

    public static void prevsong() throws Exception
    {
        clip.stop();
        clip.close();
        clip = null;

        i = i-1;
        if (i <0) i = listOfFiles.length - 1;
        musicfilepath = loadfolder + "\\" + listOfFiles[i].getName();

        soundFile = new File(musicfilepath);
        sound = AudioSystem.getAudioInputStream(soundFile);
        info = new DataLine.Info(Clip.class, sound.getFormat());
        clip = (Clip) AudioSystem.getLine(info);
        clip.open(sound);

        clip.start();
        playing = true;
        //         System.out.println("----------------------------------------------------------------------");
        //         System.out.println("Song " + i + " ,'" + listOfFiles[i].getName() + "' started playing");
        //         System.out.println("----------------------------------------------------------------------");

        GUIPlayerOrig.label.setText("Song " + i + " ,'" + listOfFiles[i].getName() + "' started playing");
    }

    public static void debugger() throws Exception
    {
        try{
            //Strinloadf = "S:\\Computer Science\\Student_Shared_Area\\2015 16 submissions\\AS mini project 5\\Shaun C - MusicPlayer - MP5\\Log File.txt";
            //Runtime.getRuntime().exec("notepad.exe" + loadf);
            String tango = "Log File (" + name + ").txt";

            Runtime runtime = Runtime.getRuntime();
            Process process = runtime.exec("notepad.exe S:\\Computer Science\\Student_Shared_Area\\2015 16 submissions\\AS mini project 5\\Shaun C - MusicPlayer - MP5\\" + tango);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
}
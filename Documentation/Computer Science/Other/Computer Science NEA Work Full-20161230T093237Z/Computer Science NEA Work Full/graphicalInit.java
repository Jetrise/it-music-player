import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.util.concurrent.TimeUnit;//for time
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.SwingConstants;
import javax.swing.JLabel;

import java.awt.*;
import java.awt.event.*;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JMenuBar;
import javax.swing.KeyStroke;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JFrame;
import javax.swing.JSlider;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Class: graphicalInit
 * Description: Graphical Display Init
 * Author: Shaun Cockram
 * 
 * PARAMETERS:
 * 
 * RETURNS:
 * 
 */
public class graphicalInit
{
    JFrame frame;
    JMenuItem itemC, itemL, itemPlB, itemEP, debugger;
    public boolean RIGHT_TO_LEFT = false;
    JButton playButton;
    JButton stopButton;
    JButton nextButton;
    JButton prevButton;
    JLabel label;

    public void addComponentsToPane(Container contentPane) { 

       contentPane.setLayout(new BorderLayout(5,5));
        if (!(contentPane.getLayout() instanceof BorderLayout)) {
            contentPane.add(new JLabel("Container doesn't use BorderLayout!"));
            return;
        }

        if (RIGHT_TO_LEFT) {
            contentPane.setComponentOrientation(
                java.awt.ComponentOrientation.RIGHT_TO_LEFT);
        }

        JButton jbnSampleButtons = new JButton("");
  
        ImageIcon pause = new ImageIcon("App/Resources/Pause.png");
        jbnSampleButtons = new JButton(pause);       
        JPanel jpCenter = new JPanel();
        jpCenter.add(jbnSampleButtons);

        ImageIcon play = new ImageIcon("App/Resources/Play.png");
        playButton = new JButton(play);
        jpCenter.add(playButton);

        ImageIcon stop = new ImageIcon("App/Resources/Stop.png");
        stopButton = new JButton(stop);
        jpCenter.add(stopButton);


        jpCenter.add(jbnSampleButtons);     
        contentPane.add(jpCenter, BorderLayout.CENTER);


        ImageIcon back = new ImageIcon("App/Resources/StepBack.png");
        prevButton = new JButton(back);
        contentPane.add(prevButton, BorderLayout.LINE_START);

        ImageIcon foward = new ImageIcon("App/Resources/StepFoward.png");
        nextButton = new JButton(foward);
        contentPane.add(nextButton, BorderLayout.LINE_END);


        JFrame f = new JFrame();
        JMenuBar menubar = new JMenuBar();
        JMenu file = new JMenu("File");

        menubar.add(file);
        f.setJMenuBar(menubar);

        itemC = new JMenuItem("Create");
        file.add(itemC);

        itemL = new JMenuItem("Load");
        file.add(itemL);

        itemEP = new JMenuItem("Example Load");
        file.add(itemEP);

        JMenu options = new JMenu("Options");
        menubar.add(options);
        f.setJMenuBar(menubar);
        debugger = new JMenuItem("Debug Log File");
        options.add(debugger);

        contentPane.add(menubar, BorderLayout.NORTH);
        label = new JLabel("Status: Nothing");
        
        contentPane.add(label, BorderLayout.PAGE_END);
    }
    public void createAndShowGUI() {
        JFrame.setDefaultLookAndFeelDecorated(true);
        frame = new JFrame("ComSci: Music Player");
        frame.setSize(600, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addComponentsToPane(frame.getContentPane());
        //frame.pack();
        frame.setVisible(true);
        frame.setResizable(false);

    }
}

import java.util.concurrent.TimeUnit;//for time
import java.util.Scanner; //Used for user inputs
import java.io.File; //used for file chooser
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import java.awt.*;
import javax.swing.*;

import javafx.scene.media.AudioClip;

import java.awt.event.*;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;

/**
 * Class: Audio Controller
 * Description: Handles user interaction with music related buttons. (Play, Pause, Stop, Skip etc)
 * Author: Shaun Cockram
 * 
 */

public class audioController 
{
    // instance variables - replace the example below with your own
    private double songVol;
    private int songDur;
    private String songDir;
    private AudioInputStream songSound;
    private DataLine.Info songInfo;
    private boolean songStatus;
    private String SONGROOT;
    private int songID;
    private Song song;
    

    /**
     * Constructor for objects of class audioController
     */
    public audioController()
    {        
        generalController gController = new generalController(); 
        this.song=new Song();
        // initialise instance variables
        songStatus = false;
        songVol = 0.75;
        String APPROOT = gController.getAppRootDir();
        SONGROOT = APPROOT + "\\Data\\Songs\\"; 
        System.out.println(SONGROOT);
       
    }


    public boolean getSongIsPlaying()
    {
        return songStatus;
    }

    public double setSongVolume(double x)
    {
        //set song volume = x.
        return songVol;
    }
    //Audio Methods
    public void playSong()
    {
        //get song id from current song
        AudioClip song = new AudioClip(SONGROOT + songID + ".mp3");
        song.play();

    }

    public void stopSong()
    {

    }

    public void pauseSong()
    {

    }

    public void skipSong()
    {

    }
}

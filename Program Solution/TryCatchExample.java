class TryCatchExample {
   public static void main(String args[]) {
     int num1, num2;
     try { 
        num1 = 0;
        num2 = 1 / num1;
     } catch (ArithmeticException e) { 
            System.out.println("Error: Cannot divide a number by zero");
     }
   }
}
//import java.net.URL; not needed
import java.io.File;
/**
 * Class Name: Main
 * Description: Program Init
 * Author: Shaun Cockram
 * 
 * PARAMETERS:
 * 
 * RETURNS:
 * 
 */

public class Main
{
    //final String APPROOT; 
    public static void Main() throws Exception
    {
        //Creating instance of GUI and then showing it. 
        graphicalInit gInit = new graphicalInit();
        gInit.createAndShowGUI();
        
        //Creating instance of generalController. 
        generalController gController = new generalController(); 
        
        //Creating instance of audioController. 
        audioController aController = new audioController(); 
        
        //Creating instance of databaseInit. 
        databaseInit dbI = new databaseInit(); 
    }
}


/**
 * Write a description of class Song here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Song
{
    // instance variables - replace the example below with your own
    private int songID;
    private String songName;
    private String songGenre;
    private String songDate;
    private String songArtist;


    public int getSongID()
    {
        // put your code here
        return songID;
    }

    public String getSongName()
    {
        // put your code here
        return songName;
    }

    public String getSongGenre()
    {
        // put your code here
        return songGenre;
    }

    public String getSongDate()
    {
        // put your code here
        return songDate;
    }

    public String getSongArtist()
    {
        // put your code here
        return songArtist;
    }
}

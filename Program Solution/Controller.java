
import java.util.concurrent.TimeUnit;//for time
import java.util.Scanner; //Used for user inputs
import java.io.File; //used for file chooser
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;

public class Controller implements ActionListener
{
    public static String name = System.getProperty("user.name");

    // public static String name = "Shaun";
    public static String excep = " ";
    public static String filename;
    public static String response = "Menu";
    public static String exisitingfolder;
    public static String loadfolder = " ";
    public static int i = 0;
    public static Clip clip;
    public static File[] listOfFiles;
    public static String musicfilepath;
    public static File soundFile;
    public static AudioInputStream sound;
    public static DataLine.Info info;
    public static boolean playing = false;
    public static String APPROOT = getAppRootDir();
    public static String filepath = APPROOT +"\\User Playlists\\"+name;
    JFrame f;

    public static String getAppRootDir()
    {
        try {
            APPROOT = System.getProperty("user.dir");
        }catch(Exception e){
            e.printStackTrace();
        }
        return APPROOT; 
    }

    public Controller (JFrame jfIn)
    {
        f = jfIn;
        GUIPlayerOrig.itemC.addActionListener(this);
        GUIPlayerOrig.itemL.addActionListener(this);
        GUIPlayerOrig.itemEP.addActionListener(this);
        GUIPlayerOrig.playButton.addActionListener(this);
        GUIPlayerOrig.stopButton.addActionListener(this);
        GUIPlayerOrig.nextButton.addActionListener(this);
        GUIPlayerOrig.prevButton.addActionListener(this);
        GUIPlayerOrig.debugger.addActionListener(this);

    }

    public void actionPerformed(ActionEvent e)  { 
        if (e.getSource() == GUIPlayerOrig.itemC) 

            try{
                createfolder(); //calling create folder method
            }
            catch (Exception ex){}

        if (e.getSource() == GUIPlayerOrig.itemL) 

            try{
                loadfolder(); //calling load folder method
            }
            catch (Exception ex){}

        if (e.getSource() == GUIPlayerOrig.itemEP) 

            try{
                loadfolderexample(); //calling load folder method
            }
            catch (Exception ex){}

        if (e.getSource() == GUIPlayerOrig.playButton) 

            try{
                playsong(); //calling load folder method
            }
            catch (Exception ex){}

        if (e.getSource() == GUIPlayerOrig.stopButton) 

            try{
                stopsong(); //calling load folder method
            }
            catch (Exception ex){}

        if (e.getSource() == GUIPlayerOrig.nextButton) 

            try{
                nextsong(); //calling load folder method
            }
            catch (Exception ex){}

        if (e.getSource() == GUIPlayerOrig.prevButton) 

            try{
                prevsong(); //calling load folder method
            }
            catch (Exception ex){}

        if (e.getSource() == GUIPlayerOrig.debugger) 

            try{
                debugger(); //calling load folder method
            }
            catch (Exception ex){}

        //This opens file explorer on a specific path.

    }

    public static void createfolder() throws Exception
    {

        /**
         * Below is essentially how you make a folder. However, it also allows you to delete previous folders with the name that you inputted at the vry start.
         */ 
        File f = new File(filepath);
        do
        {

            //String filepath = "E:\\MiniProject\\" + name; //HOME TESTING

            try{
                if(f.mkdir()) { 
                    logfile("fcreate", "a");
                    excep = "Pass";
                } else {
                    JDialog.setDefaultLookAndFeelDecorated(true);
                    int response = JOptionPane.showConfirmDialog(null, "Folder could not be created. This may be because there is already a folder with that name. Do you want to delete the folder with your current inputted name and create a new foler?", "Error", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                    if (response == JOptionPane.NO_OPTION) {
                        System.out.println("Please delete the foler manually or alternatively, re-run the program and use a different name.");
                    } else if (response == JOptionPane.YES_OPTION) {

                        try{
                            if(f.delete()){
                                String logstring = f.getName();
                                logfile("fdelete", logstring);

                            }else{
                                logfile("fdeletefail", "a");
                            }
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                    } else if (response == JOptionPane.CLOSED_OPTION) {
                        logfile("cpane", "a");
                    }
                    excep = "Fail";
                }
            }      catch(Exception e){
                e.printStackTrace();
            } 

            loadfolder = filepath;
        }
        while (excep.equalsIgnoreCase("Fail"));

        loadfolder = filepath;

        Runtime.getRuntime().exec("explorer.exe /select," + loadfolder);
        TimeUnit.SECONDS.sleep(1); //Doesn't run the next line of code until 1 second has passed.
        JOptionPane.showMessageDialog(null, "File Explorer has just opened. On the highlighted folder, please double click and place your music files in your folder." + "\n\n" + "They must be in WAV format and there must be at least 1 file. Once you have done that, come back to the terminal window.");
    }

    public static void loadfolder() throws Exception
    {
        try{
            loadfolder = filepath;
            File f = new File(loadfolder);
            if(f.exists() ) { 
                logfile("fload", "a");
            }
            else {
                logfile("floadfail", "a");
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadfolderexample() throws Exception
    {
        try{
            System.out.println(filepath);        
            loadfolder = APPROOT +"\\User Playlists\\"+"Example Songs";
            File f = new File(loadfolder);
            if(f.exists() ) { 
                logfile("floadexample", "a");     
            }
            else {
                logfile("floadexamplefail", "a");
            }
        }catch(Exception e) {
            e.printStackTrace();
        }

    }

    public static void playsong() throws Exception
    {
        if (playing == true) 
        {
            clip.stop();
            clip.close();
            clip = null;
        }

        File folder = new File(loadfolder);
        listOfFiles = folder.listFiles();
        int k;
        for (k = 0; k < listOfFiles.length; k++) {
            if (listOfFiles[k].isFile()) {
                //System.out.println("Song "+ k +"\t" + listOfFiles[k].getName());
            } else if (listOfFiles[k].isDirectory()) {
                //System.out.println("Directory " + listOfFiles[k].getName());
            }
        } 

        musicfilepath = loadfolder + "\\" + listOfFiles[i].getName();

        soundFile = new File(musicfilepath);
        sound = AudioSystem.getAudioInputStream(soundFile);
        info = new DataLine.Info(Clip.class, sound.getFormat());
        clip = (Clip) AudioSystem.getLine(info);
        clip.open(sound);

        clip.start();
        playing = true;
        GUIPlayerOrig.label.setText("Song " + i + " ,'" + listOfFiles[i].getName() + "' started playing");
    }

    public static void stopsong() throws Exception
    {
        clip.stop();
        TimeUnit.SECONDS.sleep(1);
        clip.close();
        clip = null;
        playing = false;
        GUIPlayerOrig.label.setText("Song " + i + " ,'" + listOfFiles[i].getName() + "' stopped playing");
    }

    public static void nextsong() throws Exception
    {
        clip.stop();     
        clip.close();
        clip = null;
        i = i + 1;
        if (i == listOfFiles.length) i=0;
        musicfilepath = loadfolder + "\\" + listOfFiles[i].getName();
        soundFile = new File(musicfilepath);
        sound = AudioSystem.getAudioInputStream(soundFile);
        info = new DataLine.Info(Clip.class, sound.getFormat());
        clip = (Clip) AudioSystem.getLine(info);
        clip.open(sound);
        clip.start();
        playing = true;
        GUIPlayerOrig.label.setText("Song " + i + " ,'" + listOfFiles[i].getName() + "' started playing");
    }

    public static void prevsong() throws Exception
    {
        clip.stop();
        clip.close();
        clip = null;

        i = i-1;
        if (i <0) i = listOfFiles.length - 1;
        musicfilepath = loadfolder + "\\" + listOfFiles[i].getName();

        soundFile = new File(musicfilepath);
        sound = AudioSystem.getAudioInputStream(soundFile);
        info = new DataLine.Info(Clip.class, sound.getFormat());
        clip = (Clip) AudioSystem.getLine(info);
        clip.open(sound);

        clip.start();
        playing = true;

        GUIPlayerOrig.label.setText("Song " + i + " ,'" + listOfFiles[i].getName() + "' started playing");
    }

    public static void debugger() throws Exception
    {
        try{
            String tango = "Log File (" + name + ").txt";
            File dir = new File(APPROOT + tango);    
            Process process = Runtime.getRuntime().exec("notepad.exe", null, dir);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void logfile(String type, String logstring) throws Exception
    {
        try{
            String logtype = type;
            String logstringfinal = logstring;
            String logfinal = ""; 
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = new Date();

            String tango = "Log File (" + name + ").txt";
            textFileWrite writer = new textFileWrite(tango, true);

            if (logtype == "fcreate")
            {
                logfinal = "DEBUG FOLDER CREATE (" + date + "): Folder Created with name: " + name;
            }
            else if (logtype == "fdelete")
            {
                logfinal = "DEBUG FOLDER DELETE (" + date + "): Folder, '" + logstring + "' is deleted!";
            }
            else if (logtype == "fdeletefail")
            {
                logfinal = "DEBUG FOLDER DELETE (" + date + "): Delete operation has failed.";
            }
            else if (logtype == "cpane")
            {
                logfinal = "DEBUG JPANEL (" + date + "): JPane was closed by user.";
            }
            else if (logtype == "fload")
            {
                logfinal = "DEBUG FOLDER LOAD (" + date + "): Folder loaded with name: " + name;
            }
            else if (logtype == "floadfail")
            {
                logfinal = "DEBUG FOLDER LOAD (" + date + "): Folder could not be loaded with name: " + name + ". This is because there is no folder called " + name + " in the directory. Please use the create option";
            }
            else if (logtype == "floadexample")
            {
                logfinal = "DEBUG FOLDER LOAD (" + date + "): Example Folder Loaded";
            }
            else if (logtype == "floadexamplefail")
            {
                logfinal = "DEBUG FOLDER LOAD (" + date + "): Example Folder could not be loaded.";
            }

            writer.writeToTextFile(logfinal);
            writer.closeFile();
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

}
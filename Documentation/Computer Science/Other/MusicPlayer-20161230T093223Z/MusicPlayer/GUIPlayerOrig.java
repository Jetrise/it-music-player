import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.util.concurrent.TimeUnit;//for time
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.SwingConstants;
import javax.swing.JLabel;

import java.awt.*;
import java.awt.event.*;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.ButtonGroup;
import javax.swing.JMenuBar;
import javax.swing.KeyStroke;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JFrame;
import javax.swing.JSlider;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class GUIPlayerOrig { 
    static JFrame frame;
    static JMenuItem itemC, itemL, itemPlB, itemEP, debugger;
    public static boolean RIGHT_TO_LEFT = false;
    static JButton playButton;
    static JButton stopButton;
    static JButton nextButton;
    static JButton prevButton;
    static JLabel label;

    //     public JFrame frame;

    public static void addComponentsToPane(Container contentPane) { 

        contentPane.setLayout(new BorderLayout(5,5));
        if (!(contentPane.getLayout() instanceof BorderLayout)) {
            contentPane.add(new JLabel("Container doesn't use BorderLayout!"));
            return;
        }

        if (RIGHT_TO_LEFT) {
            contentPane.setComponentOrientation(
                java.awt.ComponentOrientation.RIGHT_TO_LEFT);
        }

        JButton jbnSampleButtons = new JButton("");
        //         contentPane.add(jbnSampleButtons, BorderLayout.PAGE_START);
        //         String text = "Yo";
        //         JLabel myLabel = new JLabel(text);
        //         contentPane.add(myLabel, BorderLayout.PAGE_START);
        //         myLabel.setHorizontalAlignment(SwingConstants.CENTER);

        //NEW
        ImageIcon pause = new ImageIcon("Resources/Pause.png");
        jbnSampleButtons = new JButton(pause);
        //         jbnSampleButtons.setPreferredSize(new Dimension(200, 100));

        JPanel jpCenter = new JPanel();
        jpCenter.add(jbnSampleButtons);

        ImageIcon play = new ImageIcon("Resources/Play.png");
        playButton = new JButton(play);
        jpCenter.add(playButton);

        ImageIcon stop = new ImageIcon("Resources/Stop.png");
        stopButton = new JButton(stop);
        jpCenter.add(stopButton);

        //         jbnSampleButtons.setPreferredSize(new Dimension(200, 100));

        jpCenter.add(jbnSampleButtons);     
        contentPane.add(jpCenter, BorderLayout.CENTER);

        //NEW
        ImageIcon back = new ImageIcon("Resources/StepBack.png");
        prevButton = new JButton(back);
        contentPane.add(prevButton, BorderLayout.LINE_START);

        //         jbnSampleButtons = new JButton("WIP - Will be JSlider");
        //         contentPane.add(jbnSampleButtons, BorderLayout.PAGE_END);

        //NEW
        ImageIcon foward = new ImageIcon("Resources/StepFoward.png");
        nextButton = new JButton(foward);
        contentPane.add(nextButton, BorderLayout.LINE_END);

        //         Container contentPane = frame.getContentPane();
        //         contentPane.setLayout(new BorderLayout());

        //         contentPane.add(menubar, BorderLayout.NORTH);   
        //

        JFrame f = new JFrame();
        JMenuBar menubar = new JMenuBar();
        JMenu file = new JMenu("File");

        menubar.add(file);
        f.setJMenuBar(menubar);

        itemC = new JMenuItem("Create");
        file.add(itemC);

        itemL = new JMenuItem("Load");
        file.add(itemL);

        itemEP = new JMenuItem("Example Load");
        file.add(itemEP);

        JMenu options = new JMenu("Options");
        menubar.add(options);
        f.setJMenuBar(menubar);
        debugger = new JMenuItem("Debug Log File");
        options.add(debugger);

        contentPane.add(menubar, BorderLayout.NORTH);

        //new

        //         JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 100, 50);
        //         slider.setMinorTickSpacing(2);
        //         slider.setMajorTickSpacing(10);
        //         slider.setPaintTicks(true);
        //         slider.setPaintLabels(true);
        //         slider.setLabelTable(slider.createStandardLabels(10));
        //         contentPane.add(slider, BorderLayout.PAGE_END);
        // 
        //         JPanel jpBottom = new JPanel();
        //         jpBottom.add(slider, BorderLayout.PAGE_END);
        // 
        //         JSlider slider2 = new JSlider(JSlider.HORIZONTAL, 0, 100, 50);
        //         slider2.setMinorTickSpacing(5);
        //         slider2.setMajorTickSpacing(25);
        //         slider2.setPaintTicks(true);
        //         slider2.setPaintLabels(true);
        //         slider2.setLabelTable(slider.createStandardLabels(10));
        //         contentPane.add(jpBottom, BorderLayout.PAGE_END);
        // 
        //         jpBottom.add(slider2, BorderLayout.PAGE_END);



        label = new JLabel("Status: Nothing");
        
        contentPane.add(label, BorderLayout.PAGE_END);

        
        

    }
    static void createAndShowGUI() {
        JFrame.setDefaultLookAndFeelDecorated(true);
        frame = new JFrame("Mini-Project 5: Music Player");
        frame.setSize(600, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addComponentsToPane(frame.getContentPane());
        //frame.pack();
        frame.setVisible(true);
        frame.setResizable(false);

    }

    public static void makeMenuBar(){
        //         JMenuBar menubar = new JMenuBar();
        //         JMenu menu; 
        //         JMenuItem item;
        // 
        //         JMenu file = new JMenu("File");
        //         menubar.add(file);
        // 
        //         item = new JMenuItem("New Game...");
        //         file.add(item);
        // 
        //         item = new JMenuItem("Save As...");
        //         file.add(item);
        // 
        //         item = new JMenuItem("Quit");
        //         file.add(item); 
        // 
        //         JFrame f = new JFrame();
        //         JMenuBar menubar = new JMenuBar();
        //         JMenu file = new JMenu("File");
        // 
        //         menubar.add(file);
        //         f.setJMenuBar(menubar);

    }

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    createAndShowGUI();
                }
            });
    }
}